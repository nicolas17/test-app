<!DOCTYPE html>
<html>
<head>
<title>Test app</title>
</head>

<body>

<?php
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    echo '<form action="" method="post">';
    echo '<p>Enter your name:<br><input type="text" name="name"></p>';
    echo '<p><input type="submit" value="Submit"></p>';
    echo '</form>';
} else if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    echo '<p>Hello '.htmlspecialchars($_POST['name']).'</p>';
}
?>

</body>
</html>
